Version ?.?.?
-------------
 - PSG Parity optimized
 - Overscan areas now drawn with backdrop colour
 - Roller Controller support added
 - Steering Wheel support added

Version 0.4.0
-------------
 - Major optimizations in Z80 emulator
 - Many improvements to Z80 accuracy
 - Language standard changed from C99 to C11
 - Build improvements
 - Improve state serializer code

Version 0.3.1
-------------
 - Fix bug in serializer when loading the upper bits of 64-bit integers

Version 0.3.0
-------------
 - Code, build, and filesystem layout cleanups
 - More accurate SN76489AN emulation
 - Z80 fixes backported from Cega
 - Allow specifying a BIOS and loading from a memory buffer
 - Stable Save State format

Version 0.2.2
-------------
 - Relicense from MPL-2.0 to BSD-3-Clause

Version 0.2.1
-------------
 - Fix AY-3-8910 Envelope when Hold bit is set

Version 0.2.0
-------------
 - PSG Noise channel accuracy improvement
 - Z80 fixes backported from Cega
 - Optionally build with vendored or system SpeexDSP
 - Improve audio performance and adjust pitch to be more accurate
 - Super Action Controller support added
 - Fix VDP bug when EC bit is set on magnified/doubled sprites

Version 0.1.1
-------------
 - Rework mixer logic and simplify envelope code in AY-3-8910
 - Fix bug that prevented resetting Super Game Module content

Version 0.1.0
-------------
 - Emulation of the Super Game Module
 - Bugfix in SN76489AN emulation code
 - Scalable JollyCV logo icon added to the repository

Version 0.0.0
-------------
 - First public release
 - Emulation of the basic NTSC/PAL ColecoVision
 - Support for Mega Carts
